

function getJson(url, ok, err)
{
    let request = new XMLHttpRequest()
    request.open("GET", url);
    request.onerror = () => getJson(url, ok, err)
    request.onload = () => ok(request.responseText);
    request.send()
}

function createExpansion(data, i)
{
    let state = {}
    let view = document.getElementById("app")
    let fragment = document.getElementById("ProjectTemplate").content.cloneNode(true);
   
    view.appendChild(fragment)
    // Get the instanciated fragment
    let node = view.children[i]

    node.querySelector(".projectTitle").innerHTML = data.title
    node.querySelector(".projectInvestigator").innerHTML = data.pi
    node.querySelector(".projectInstitution").innerHTML = data.organization
        
    if (data.type && data.typeId)
        node.querySelector(".projectId").innerHTML = data.type + "-" + data.typeId;
    
    let status = node.querySelector(".Status")
    status.innerHTML = data.status
    if (data.status === "Active") {
        status.classList.add("Active")
    }
    else if (data.status === "Completed") {
        let anchor = node.querySelector(".finalProjectReport") 
        anchor.innerHTML = "Final Project Report"
        anchor.setAttribute("href", data.finalReport)
        status.classList.add("Completed")
    }
    else {
        status.classList.add("UnknownStatus")
    }
        
    if (data.status !== "Completed") {
        node.querySelector(".finalProjectButton").remove()
    }

    let infopane = node.querySelector(".ProjectData")
    let expand_bt = node.querySelector(".expand")
    let hidden = true;
    infopane.style.display = "none"
    infopane.innerHTML = data.innerHTML
   
    node.style.display = "block"

    // Calllback when user presses "See "
    expand_bt.onclick = () =>
    {
        infopane.style.display = hidden ? "block": "none";
        hidden = !hidden;
    }

   
    // A method of this object    
    let set_vis = (vis) =>
    { 
        if (vis) {
            node.style.display = "block"
        }
        else {
            node.style.display = "none"
        }
    }

    // Another method
    let is_vis = () => 
    {
        return node.style.display === "block"
    }

    let getStatus = () =>
    {
        return data.status 
    }
    
    // The instance of this class
    let props = {
        title: data.title,
        type: data.type,
        qualname: data.type + "-" + data.typeId,
        typeid:parseInt(data.typeId ? data.typeId : "0"),
        set_vis,
        is_vis,
        getStatus,
    }
    
    typefilters[data.type] = data.typeName
    return props
        
}
// Populated by callback
let typefilters = {
    
}

let projects = []
let currentTypeFilter = () => {}
let currentStatusFilter = () => {}

document.predicates = {
    "type": (elt) => true,
    "status": (elt) => true,
}

function projectShouldBeVisible(project) 
{
    for (let predicate of Object.values(document.predicates)) {
        if (! predicate(project)) {
            return false
        }
             
    }
    return true;
}

function updateProjectVisibility()
{
 
    console.log("updateProjectVisibility")
    let nameset = {}
    for (let project of projects) {
        if (project.title in nameset) {
            console.log(project.ttle)
            continue
        }
        nameset[project.title] = project.title

       let vis = projectShouldBeVisible(project) 
        //console.log(project.title + " " + vis) 
       project.set_vis(vis)
    }
}

function onData(data)
{
    let body = JSON.parse(data)
    let app = document.getElementById("app"); 
    // This also populates type filters
    
    body.data.sort((a, b) =>
        {
            a_id = a.type + '-' + a.typeId;
            b_id = b.type + '-' + b.typeId;
            if (a_id < b_id) return -1;
            if (b_id < a_id) return 1;
            return 0;
        })
   
    projects = body.data.map(createExpansion);
 
    let filter_node = document.getElementById("filter")

    let create_filter = (key) =>
    {
        
        let func = () => {
            document.predicates["type"] = (obj) => obj.type === key 
            console.log(document.predicates)
            updateProjectVisibility()            
        }
        return func
    }

    

    for (let filter in typefilters) {
        let name = typefilters[filter]
        let bt = document.createElement("button")
        
        bt.innerHTML = name ? name : "All Projects"
     
        bt.onclick = create_filter(filter)
        if (bt.innerHTML === "All Projects") {  
            console.log("All Projects")
            bt.onclick = () => { 
                document.predicates["type"] = () => true;
                updateProjectVisibility();
            }
        }
        filter_node.appendChild(bt)
    }

    

}


function showAll()
{
        projects.forEach(elt => elt.set_vis(true))
}

function statusAll_handler()
{
    document.predicates["status"] = () => true;
}

function initRadio()
{
    let statusAll = document.getElementById("status-All")
    let statusActive = document.getElementById("status-Active")
    let statusComplete = document.getElementById("status-Completed")
    

    statusComplete.onclick = () =>
    {
        document.predicates["status"] = (obj) => obj.getStatus() === "Completed"
        console.log(document.predicates)
        updateProjectVisibility()
    }
    
    statusActive.onclick = () =>
    {
        document.predicates["status"] = (obj) => obj.getStatus() === "Active"
        console.log(document.predicates)
        updateProjectVisibility()
    }
}


getJson("https://inspire-utc.git-pages.mst.edu/rip-trb/static/projects.json", onData, () => {})
initRadio()
